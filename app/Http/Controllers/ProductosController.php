<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\Models\ProductosModel;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('busqueda/index');
    }

    public function productosmas()
    {
        $cadena = "";
        $res = DB::select("SELECT id_producto,titulo,sum(busquedas_estadisticas) as CUENTA FROM pahumadaPost.table_estadisticas
        LEFT JOIN table_productos ON id_producto = producto_estadistica GROUP BY table_productos.id_producto ORDER BY CUENTA DESC LIMIT 20");

        foreach($res as $data){

            $cadena =$cadena.'<tr><td>'.$data->titulo.'</td><td>'.$data->CUENTA.'</td><td>'.$this->palabrasestadistica($data->id_producto)."</td></tr>|";
        }

        return view('busqueda/productosmas')->with('estadisticas',$cadena);
    }

    public function palabrasestadistica($id)
    {
        $pal='';

        $res = DB::select("SELECT palabra_estadistica FROM pahumadaPost.table_estadisticas
        WHERE producto_estadistica = ".$id);


        foreach ($res as $palabras) {
            $pal = $palabras->palabra_estadistica;
        }

        return $pal;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function estadisticas(){

        $pal = $_POST['palabra'];
        $pro = $_POST['producto'];

        $prod_count = DB::table('table_estadisticas')
            ->where('palabra_estadistica', '=', $pal)
            ->where('producto_estadistica', '=', $pro)
            ->count();

        if($prod_count==0){
            DB::table('table_estadisticas')->insert([
                ['palabra_estadistica' => $pal,
                'producto_estadistica' => $pro,
                'busquedas_estadisticas' => 1]
            ]);
        }else{
            $res = DB::select("SELECT id_estadistica,
                                    palabra_estadistica,
                                    producto_estadistica,
                                    busquedas_estadisticas
                            FROM table_estadisticas
                            WHERE palabra_estadistica ='".$pal."' AND producto_estadistica ='".$pro."'" );
            foreach($res as $registro){

                DB::table('table_estadisticas')
                    ->where('id_estadistica', $registro->id_estadistica)
                    ->update(['busquedas_estadisticas' => $registro->busquedas_estadisticas + 1]);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search($prod)
    {
        if(!isset($prod)){
            $prod=$_POST['data'];
        }

        $productos =array();
        $results = DB::select("SELECT id_producto,
                titulo,
                descripcion,
                fecha_inicio,
                fecha_termino,
                precio,
                imagen,
                vendidos,
                tags
                FROM table_productos
            WHERE titulo LIKE '%".$prod."%'
                LIMIT 50 ");
        return $results;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
