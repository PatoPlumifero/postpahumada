<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/estadisticas/nuevo','ProductosController@estadisticas' );
Route::get('/busqueda','ProductosController@index' );

Route::get('/busqueda/productosmas','ProductosController@productosmas' );
Route::post('/busqueda/tablaestadistica','ProductosController@tablaestadistica' );


