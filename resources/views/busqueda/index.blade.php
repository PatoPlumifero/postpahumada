<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Busqueda</title>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script>

        $(document).ready(function(){

            var url = window.location.protocol + "//" + window.location.host + "/";
            $("#buscar").click(function() {

                if($('#txt_buscador').val() != '')
                {
                    parametros = $('#txt_buscador').val();
                    var celdas;
                    $.getJSON( url + "/api/productos/search/"+parametros,
                    function(data){

                        for (var i = 0, len = data.length; i < len; i++) {
                            indice=i+1;
                            celdas = celdas + '<tr><td>'+ indice + '</td> <td>'+ data[i].titulo +'</td>' + '<td>'+ data[i].descripcion +'</td>' + '<td>'+ data[i].fecha_inicio +'</td>' + '<td>'+ data[i].fecha_termino +'</td></tr>';

                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: url + 'estadisticas/nuevo',
                                data: { 'palabra' : parametros , 'producto' : data[i].id_producto },
                                type: 'post',
                                async:true,
                                success: function(response){
                                    console.log(response);
                                },
                                error: function(response){
                                    console.log(response);
                                }
                            });


                        }

                        $('#resultado').html(celdas);

                    });

                }else{
                    alert('Debe ingresar elemento a buscar')
                }

            });
        });



           

</script>
</head>
<body>

    <br>
    <div>
        <label>Buscador</label>
    </div>
    <br>
    <div>
        <input type="text" id="txt_buscador" name="txt_buscador">
    </div>
    <br>
    <div>
        <button id="buscar" name="buscar" type="button">BUSCADOR</button>
    </div>

    <div>
        <br>
    <table border="1px">
        <thead>
            <th>Indice</th>
            <th>titulo</th>
            <th>detalle</th>
            <th>inicio</th>
            <th>termino</th>
        </thead>
        <tbody id="resultado">

        </tbody>
    </table>
</div>

</body>
</html>
